package sa.edu.kau.fcit.cpit252;

import java.util.Arrays;

public final class VirtualMachine {
    private  String cloudProvider;
    private  String region;
    private  String az;
    private  String imageId;
    private  String instanceClass;
    private  String vpc;
    private  String subnet;
    private  String securityGroup;
    private  String publicIP;
    private  String[] ports;
    private  String privateIP;
    private  String placementGroup;
    private  String architecture;
    private  String hypervisor;
    private  String storage;
    private  String[] tags;
    
    private VirtualMachine(Builder builder){
    this.cloudProvider = builder.cloudProvider;
    this.region = builder.region;
    this.az = builder.az;
    this.imageId = builder.imageId;
    this.instanceClass = builder.instanceClass;
    this.vpc = builder.vpc;
    this.subnet = builder.subnet;
    this.securityGroup = builder.securityGroup;
    this.publicIP = builder.publicIP;
    this.ports = builder.ports;
    this.privateIP = builder.privateIP;
    this.placementGroup = builder.placementGroup;
    this.architecture = builder.architecture;
    this.hypervisor = builder.hypervisor;
    this.storage = builder.storage;
    this.tags = builder.tags;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports,
                          String privateIP, String placementGroup) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports,
                          String privateIP, String placementGroup, String architecture) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP,
                          String[] ports, String privateIP, String placementGroup, String architecture, String hypervisor) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[]ports,
                          String privateIP, String placementGroup, String architecture, String hypervisor, String storage) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
        this.storage = storage;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[]ports, String privateIP,
                          String placementGroup, String architecture, String hypervisor, String storage, String[] tags) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
        this.storage = storage;
        this.tags = tags;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VirtualMachine{" +
                "cloudProvider='" + cloudProvider + '\'' +
                ", region='" + region + '\'' +
                ", az='" + az + '\'' +
                ", imageId='" + imageId + '\'' +
                ", instanceClass='" + instanceClass + '\'' +
                ", vpc='" + vpc + '\'' +
                ", subnet='" + subnet + '\'' +
                ", securityGroup='" + securityGroup + '\'' +
                ", publicIP='" + publicIP + '\'');
        if (ports != null) {
            sb.append(", ports=" + Arrays.toString(ports));
        }
        if (privateIP != null) {
            sb.append(", privateIP='" + privateIP + '\'');
        }
        if (placementGroup != null) {
            sb.append(", placementGroup='" + placementGroup + '\'');
        }
        if (architecture != null) {
            sb.append(", architecture='" + architecture + '\'');
        }
        if (hypervisor != null) {
            sb.append(", hypervisor='" + hypervisor + '\'');
        }
        if (storage != null) {
            sb.append(", storage='" + storage + '\'');
        }
        if (tags != null) {
            sb.append(", tags=" + Arrays.toString(tags));
        }
        sb.append("}");
        return sb.toString();
    }
    public static class Builder {

        private String cloudProvider;
        private String region;
        private String az;
        private String imageId;
        private String instanceClass;
        private String vpc;
        private String subnet;
        private String securityGroup;
        private String publicIP;
        private String[] ports;
        private String privateIP;
        private String placementGroup;
        private String architecture;
        private String hypervisor;
        private String storage;
        private String[] tags;

        public Builder(String aws, String useast, String az1, String ubuntu2004098im, String t2micro, String myvpc, String mysubnet, String sg1, String myvmpubkey) {
        }

        public Builder withcloudProvider(String cloudProvider) {
            this.cloudProvider = cloudProvider;
            return this;
        }

        public Builder withregion(String region) {
            this.region = region;
            return this;
        }

        public Builder withaz(String az) {
            this.az = az;
            return this;
        }

        public Builder withimageId(String imageId) {
            this.imageId = imageId;
            return this;
        }

        public Builder withinstanceClass(String instanceClass) {
            this.instanceClass = instanceClass;
            return this;
        }

        public Builder withvpc(String vpc) {
            this.vpc = vpc;
            return this;
        }

        public Builder withsubnet(String subnet) {
            this.subnet = subnet;
            return this;
        }

        public Builder withsecurityGroup(String securityGroup) {
            this.securityGroup = securityGroup;
            return this;
        }

        public Builder withpublicIP(String publicIP) {
            this.publicIP = publicIP;
            return this;
        }

        public Builder withPorts(String ports[]) {
            this.ports = ports;
            return this;
        }

        public Builder withprivateIP(String privateIP) {
            this.privateIP = privateIP;
            return this;
        }

        public Builder withplacementGroup(String placementGroup) {
            this.placementGroup = placementGroup;
            return this;
        }

        public Builder witharchitecture(String architecture) {
            this.architecture = architecture;
            return this;
        }

        public Builder withhypervisor(String hypervisor) {
            this.hypervisor = hypervisor;
            return this;
        }

        public Builder withstorage(String storage) {
            this.storage = storage;
            return this;
        }

        public Builder withTags(String tags[]) {
            this.tags = tags;
            return this;
        }

        public VirtualMachine build() {
            return new VirtualMachine(this);
        }

    }
}
