package sa.edu.kau.fcit.cpit252;

public class App 
{
    public static void main( String[] args )
    {
        // Create a VM on AWS, us-east, az-1, ubuntu20.04-098im, t2.micro, myvpc, mysubnet,
        // sg1, my-vm-pub-key.
        VirtualMachine awsVM = new VirtualMachine("AWS", "us-east", "az-1",
                "ubuntu20.04-098im", "t2.micro", "myvpc",
                "mysubnet", "sg1", "my-vm-pub-key");
        System.out.println("launching a VM on AWS \n" + awsVM);

        // Create a VM on Azure, us-west, az-1, windows-server.2022-5483im, a.series, myvpc, mysubnet,
        // sg1, my-vm-pub-key, ports [3389, 80, 443], and tags ["cpit252", "lab"]
        VirtualMachine azureVM = new VirtualMachine("Azure", "us-west", "az-1",
                "ubuntu20.04-098im", "a.series", "myvpc",
                "mysubnet", "sg1", "my-vm-pub-key", new String[]{"3389", "80", "443"},
                null, null, null, null, null,
                new String[]{"cpit252", "lab"});
        System.out.println("launching a VM on Azure \n" + azureVM);
    }
}
