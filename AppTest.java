package sa.edu.kau.fcit.cpit252;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;


public class AppTest 
{
    @Test
    public void shouldCreateVMWithRequiredParameters()
    {
        VirtualMachine awsVM = new VirtualMachine.Builder("AWS", "us-east", "az-1",
                "ubuntu20.04-098im", "t2.micro", "myvpc",
                "mysubnet", "sg1", "my-vm-pub-key")
                .build();
        assertNotNull(awsVM);
    }

    @Test
    public void shouldCreateVMWithOptionalParameters()
    {
        VirtualMachine awsVM = new VirtualMachine.Builder("AWS", "us-east", "az-1",
                "ubuntu20.04-098im", "t2.micro", "myvpc",
                "mysubnet", "sg1", "my-vm-pub-key")
                .withPorts(new String[]{"3389", "80", "443"})
                .withTags(new String[]{"cpit252", "lab"})
                .build();
        assertNotNull(awsVM);
    }
}
